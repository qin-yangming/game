import socket
import threading
import re
import time
from poker_card import *
import gevent
from gevent import monkey
monkey.patch_all()
player = [Player(5)]
player_list = []
ids = []
names = []
sockets = []
dect = {}
dect_temp = {1: ('a', '100'),2: ('a', '100'),3: ('a', '100'),4: ('a', '100'),
             5: ('a', '100'),6: ('a', '100'),7: ('a', '100'),8: ('a', '100')}


# 广播msg信息
def public_send(msg):
    msg = str(msg)
    for s in sockets:
        s.send(msg.encode('utf-8'))


# 用户登录
def login(data, client_socket, client_id):
    ret = re.match(r"用户名：(.*?)密码：(.*)", data)
    if ret:  
        name, psw = ret.group(1), ret.group(2)
        # 登录已有账号
        if name in dect:
            if psw == dect[name][0]:
                if name in names and name != 'AI':
                    client_socket.send(f"该用户已登录！请换个用户试试".encode('utf-8'))
                    return 0
                names.append(name)
                dect_temp[client_id] = (name, dect[name][1])
                client_socket.send(f"登录成功！剩余筹码数为：{dect[name][1]}".encode('utf-8'))
                client_socket.send(("你的ID：" + str(client_id)).encode('utf-8'))
                ids.append(client_id)
                ids.sort()
                s = "玩家名字："
                for i in ids:
                    s += "@" + str(dect_temp[i][0]) + "@"
                s += "每人筹码："
                for i in ids:
                    s += "$" + str(dect_temp[i][1])
                public_send(s)
                return 1
            else:
                client_socket.send(f"用户密码错误，请重新输入".encode('utf-8'))
        # 注册新账号
        else:
            client_socket.send(f"不存在该用户，是否注册该用户yes/no?".encode('utf-8'))
            data = client_socket.recv(1024)
            if data:
                data = str(data.decode("utf-8"))  # 解码
            if data == 'yes' or data == "Yes":  # 注册新用户
                with open("user.txt", 'a', encoding='utf-8') as f:
                    f.write("\n")
                    f.write(name + ":" + ret.group(2) + ":" + "200")
                    dect[name] = (ret.group(2), 500)  # 增加键值对
                    client_socket.send(f"注册成功，请重新登录".encode('utf-8'))
            else:
                client_socket.send(f"请输入正确的账户和密码".encode('utf-8'))
    return 0


def is_game_over():
    len_temp = len(player_list)
    if len_temp == 1:
        Player.state = 3
        return 1
    if Player.count_call == len_temp:
        return 1
    else:
        return 0


def state_action():
    if Player.state == 1:
        public_send("前三张公共牌为：" + str(player[0].addr_list0[0]) + "，" + str(player[0].addr_list0[1]) + "，" + str(player[0].addr_list0[2]))
    elif Player.state == 2:
        public_send("第四张公共牌为：" + str(player[0].addr_list0[3]))
    elif Player.state == 3:
        public_send("第五张公共牌为：" + str(player[0].addr_list0[4]))
    elif Player.state == 4 and len(player_list) == 1:
            public_send("前三张公共牌为：" + str(player[0].addr_list0[0]) + "，" + str(player[0].addr_list0[1]) + "，" + str(player[0].addr_list0[2]))
            public_send("第四张公共牌为：" + str(player[0].addr_list0[3]))
            public_send("第五张公共牌为：" + str(player[0].addr_list0[4]))


def get_winner():
    for i in range(1, Player.num + 1):
        public_send("玩家"+str(i)+"的手牌是：" + str(player[i].addr_list0[0]) + '，'+str(player[i].addr_list0[1]))
    len_temp = len(player_list)
    for i in player_list:
        player[i].rank_power = Player.rank(player[i].hand, Player.public_card)   # 计算剩余玩家的牌力值
        print(player[i].rank_power)
        Player.get_best_poker(player[i])
        print(i, player[i].best_rank)
        sockets[i-1].send(str(player[i].best_rank).encode("utf-8"))
    best_rank_power = player[player_list[0]].rank_power
    best_rank = player[player_list[0]].best_rank
    for i in range(1,len_temp):
        if Player.is_higher_rank(player[player_list[i]].best_rank, best_rank):
            best_rank = player[player_list[i]].best_rank
        if player[player_list[i]].rank_power > best_rank_power:   # 计算牌力值
            best_rank_power = player[player_list[i]].rank_power
    Player.winner = []
    for i in range(0, len_temp):
        if player[player_list[i]].rank_power == best_rank_power:
            Player.winner.append(player_list[i])
    public_send("获胜玩家" + str(Player.winner[0]) + "的最佳牌型为" + str(best_rank))
    if len(Player.winner) == 1:
        public_send("玩家" + str(Player.winner[0]) + "获得胜利！" + str(best_rank[0][2:]))
    elif len(Player.winner) == 2:
        public_send("玩家" + str(Player.winner[0]) +"和玩家" + str(Player.winner[1]) + "平局！" + str(best_rank[0][2:]))
    else:
        public_send("玩家" + str(Player.winner[0]) + "玩家" + str(Player.winner[1]) + "玩家" + str(Player.winner[2]) + "平局！" + str(best_rank[0][2:]))


#  判断游戏是否结束，本轮游戏是否结束，不是的话继续游戏，要下一位玩家行动
def game_action(client_id):
    if is_game_over():  # 本轮游戏结束
        Player.state += 1
        Player.count_call = 0
        Player.call_money = 0
        state_action()
        if Player.state != 4:  # 下一轮投注阶段 从庄家左手边开始投注
            n = len(player_list)
            for i in range(n):
                if player_list[i] > Player.banker_pos:
                    public_send("玩家" + str(player_list[i]) + "行动中，" + str(Player.call_money))
                    break
                elif i == n - 1:
                    public_send("玩家" + str(player_list[0]) + "行动中，" + str(Player.call_money))
        else:  # 摊牌阶段
            get_winner()
    else:
        n = len(player_list)
        for i in range(n):
            if player_list[i] > int(client_id):
                public_send("玩家" + str(player_list[i]) + "行动中，" + str(Player.call_money))
                break
            elif i == n - 1:
                public_send("玩家" + str(player_list[0]) + "行动中，" + str(Player.call_money))


def action_msg(data, client_id):
    if data == "准备好了":
        Player.cnt = Player.cnt + 1
        return
    if data == "弃牌":
        player_list.remove(client_id)
        public_send("玩家" + str(client_id) + "的动作为：弃牌")
        game_action(client_id)
    ret = re.match(r"跟注，([0-9]*)", data)
    if ret:
        Player.count_call += 1
        public_send("玩家" + str(client_id) + "的动作为：" + data)
        game_action(client_id)
    ret = re.match(r"加注，([0-9]*)", data)
    if ret:
        Player.count_call = 1
        Player.call_money = ret.group(1)
        public_send("玩家" + str(client_id) + "的动作为：" + data)
        game_action(client_id)
    ret = re.findall(r"\$([0-9|\-]*)", data)
    if ret:
        for i in range(len(ret)):
            name = dect_temp[i + 1][0]
            dect[name] = (dect[name][0], ret[i])
        with open("user.txt", "w", encoding="utf-8") as f:
            for key, value in dect.items():
                s = key + ":" + value[0] + ":" + value[1] + "\n"
                f.write(s)


# 与客户端进行通信
def service_client(client_socket, client_id):
    client_socket.send("连接成功！".encode("utf-8"))
    sockets.append(client_socket)
    is_login = 0
    while True:
        data = client_socket.recv(1024).decode('utf-8')
        if data:
            print("收到玩家" + str(client_id) + "的信息为：" + data)
        if is_login == 0:
            if login(data, client_socket, client_id):
                is_login = 1
        else:   # 登录后
            action_msg(data, client_id)
            

# 用户初始化
def user_init():
    with open("user.txt", 'r', encoding="utf-8") as f:
        for line in f:
            s = line.rstrip()  # 读取每一行信息
            temp = re.match(r'(.*):(.*):(.*)', s)
            if temp:
                dect[temp.group(1)] = (temp.group(2), temp.group(3))
    print(dect)


def start_game():
    Player.cnt = 0
    global player_list
    public_send("开始游戏")
    Player.banker_pos = Player.banker_pos % Player.num + 1  # 庄家位置
    Player.small_blind_pos = Player.small_blind_pos % Player.num + 1    # 小盲注位置
    Player.big_blind_pos = Player.big_blind_pos % Player.num + 1        # 大盲注
    public_send("玩家" + str(Player.banker_pos) + "为庄")
    Player.call_money = Player.small_blind * 2      # 大盲注
    Player.state = 0
    Player.count_call = 0
    Player.cards_main = Player.deck.copy()
    player[0].renew(5)          # 公共牌
    player_list = []
    for i in range(1, Player.num + 1):
        player_list.append(i)
        player[i].renew(2)
    for i in range(1, Player.num + 1):
        sockets[i-1].send(("玩家" + str(i) + "的手牌是：" + str(player[i].addr_list0[0]) + '，' + str(player[i].addr_list0[1])).encode('utf-8'))
    public_send("玩家" + str(Player.small_blind_pos) + "下小盲注：" + str(Player.small_blind) + "，玩家"+ str(Player.big_blind_pos) + "下大盲注：" + str(Player.call_money))
    if Player.big_blind_pos + 1 <= Player.num:
        public_send("玩家" + str(Player.big_blind_pos + 1) + "行动中，" + str(Player.call_money))
    else:
        public_send("玩家1行动中，" + str(Player.call_money))


def game_run():
    while True:
        if Player.cnt >= Player.num >= 2:
            print("发送了开始游戏n=%d,cnt=%d" % (Player.num, Player.cnt))
            start_game()
            print("发送后开始游戏n=%d,cnt=%d" % (Player.num, Player.cnt))
        time.sleep(0.1)


def main():
    # 用户信息初始化
    user_init()
    # 1.创建套接字
    tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # 2.绑定本地的信息 ip 及端口
    tcp_server_socket.bind(('', 6666))
    # 3.让默认的套接字由主动变为被动 listen
    tcp_server_socket.listen(128)
    gevent.spawn(game_run)
    print('等待玩家进入中...')
    player_num = 0
    while True:
        new_client_socket, client_addr = tcp_server_socket.accept()
        player_num += 1
        print("客户端%d来了:" % player_num + str(client_addr))
        gevent.spawn(service_client, new_client_socket, player_num)
        player.append(Player(2))  # 玩家手牌
        Player.num = player_num
        player_list.append(player_num)


if __name__ == '__main__':
    main()
