import random
#玩家类
class Player():
    c_array = [(0,1,2,3,4),(0,1,2,3,5),(0,1,2,3,6),(0,1,2,4,5),(0,1,2,4,6),   # 7张牌中选5张
               (0,1,2,5,6),(0,1,3,4,5),(0,1,3,4,6),(0,1,3,5,6),(0,1,4,5,6),
               (0,2,3,4,5),(0,2,3,4,6),(0,2,3,5,6),(0,2,4,5,6),(0,3,4,5,6),
               (1,2,3,4,5),(1,2,3,4,6),(1,2,3,5,6),(1,2,4,5,6),(1,3,4,5,6),
               (2,3,4,5,6)]#7张牌里选5张牌
    deck = [(14, 4, '♠ A'), (14, 3, '♥ A'), (14, 2, '♣ A'), (14, 1, '♦ A'), (13, 4, '♠ K'), (13, 3, '♥ K'),
            (13, 2, '♣ K'), (13, 1, '♦ K'), (12, 4, '♠ Q'), (12, 3, '♥ Q'), (12, 2, '♣ Q'), (12, 1, '♦ Q'),
            (11, 4, '♠ J'), (11, 3, '♥ J'), (11, 2, '♣ J'), (11, 1, '♦ J'), (10, 4, '♠ 10'), (10, 3, '♥ 10'),
            (10, 2, '♣ 10'), (10, 1, '♦ 10'), (9, 4, '♠ 9'), (9, 3, '♥ 9'), (9, 2, '♣ 9'), (9, 1, '♦ 9'),
            (8, 4, '♠ 8'), (8, 3, '♥ 8'), (8, 2, '♣ 8'), (8, 1, '♦ 8'), (7, 4, '♠ 7'), (7, 3, '♥ 7'),
            (7, 2, '♣ 7'), (7, 1, '♦ 7'), (6, 4, '♠ 6'), (6, 3, '♥ 6'), (6, 2, '♣ 6'), (6, 1, '♦ 6'),
            (5, 4, '♠ 5'), (5, 3, '♥ 5'), (5, 2, '♣ 5'), (5, 1, '♦ 5'), (4, 4, '♠ 4'), (4, 3, '♥ 4'),
            (4, 2, '♣ 4'), (4, 1, '♦ 4'), (3, 4, '♠ 3'), (3, 3, '♥ 3'), (3, 2, '♣ 3'), (3, 1, '♦ 3'),
            (2, 4, '♠ 2'), (2, 3, '♥ 2'), (2, 2, '♣ 2'), (2, 1, '♦ 2')]
    cards_main = deck.copy()
    num = 2  #玩家人数
    computerNum = 0
    cnt = 0
    public_card = []#公共牌
    state = 0  # 游戏状态
    winner = []  # 获胜玩家
    count_call = 0  # 每一轮跟注次数
    small_blind = 1  # 小盲注
    banker_pos = 0 # 庄家
    small_blind_pos = 1 # 小盲位
    big_blind_pos = 2 # 大盲位
    call_money = 0 # 每一轮跟注钱数
    def __init__(self,num=2):
        self.hand = []#手牌
        self.poker = []#五张组合牌
        self.rank_power = 0
        self.rank = []#牌的等级
        self.best_rank = []#牌的最高等级
        self.action = 0 #玩家的行为  0-静观其变   1-弃牌   2-下注
        self.money = 999 #初始化金钱
        self.bet_money = 1#下注
        for i in range(num):  # 获取随机的2张手牌
            rand = random.choice(Player.cards_main)
            self.hand.append(rand)
            Player.cards_main.remove(rand)
            if num==5:
                Player.public_card.append(rand)
        self.cards_list = []#牌的大小
        self.suites_list = []#牌的花色
        self.addr_list0 = [card[0] + card[1] * 20 - 20 for card in self.hand]  # 牌的地址
    def renew(self,num=2):
        self.hand = []#清空手牌
        if num == 5:
            Player.public_card = []#清空公共牌
        for i in range(num):#随机获取手牌或者公共牌
            rand = random.choice(Player.cards_main)
            self.hand.append(rand)
            Player.cards_main.remove(rand)
            if num==5:
                Player.public_card.append(rand)
        self.cards_list = []
        self.suites_list = []
        self.best_rank = []
        self.addr_list0 = [card[0] + card[1] * 20 - 20 for card in self.hand]

    @staticmethod
    def get_card_power(num1, num2, num3, num4, num5, color1, color2, color3, color4, color5):  # 获得牌力一般方法
        cards_list = [num1, num2, num3, num4, num5]  # 牌值 2-A 映射到 2 -14
        suites_list = [color1, color2, color3, color4, color5]  # 花色 映射到 1，2，3，4
        cards_list.sort()  # 从小到大排序
        result = 0
        straight = 1
        for i in range(4):
            if cards_list[i] + 1 != cards_list[i + 1]:
                straight = 0
                break
        if len(set(suites_list)) == 1:  # 同花
            if straight == 1:
                result = cards_list[0]
                result = 9 << 20 | result  # 同花顺
            else:  # 同花
                for i in range(5):
                    result = result | cards_list[i] << 4 * i
                result = 6 << 20 | result
        elif cards_list.count(cards_list[2]) == 4:  # 四条
            if cards_list.count(cards_list[0]) == 1:
                result = cards_list[0] | cards_list[1]<<4
            else:
                result = cards_list[4] | cards_list[1]<<4
            result = 8 << 20 | result
        elif (cards_list.count(cards_list[0]) == 3 and cards_list.count(cards_list[4]) == 2):
            result = cards_list[2]<<4 | cards_list[4]
            result = 7 << 20 | result
        elif (cards_list.count(cards_list[0]) == 2 and cards_list.count(cards_list[4]) == 3):
            result = cards_list[2]<<4 | cards_list[0]
            result = 7 << 20 | result
        elif cards_list == [2, 3, 4, 5, 14] or straight == 1:  # 顺子
            result = cards_list[0]
            result = 5 << 20 | result
        elif cards_list.count(cards_list[2]) == 3:  # 三条
            result = cards_list[2]<<8
            if cards_list[0] == cards_list[2]:
                result = result | cards_list[3] | cards_list[4] << 4
            elif cards_list[1] == cards_list[3]:
                result = result | cards_list[0] | cards_list[4] << 4
            else:
                result = result | cards_list[0] | cards_list[1] << 4
            result = 4 << 20 | result
        elif cards_list.count(cards_list[1]) == 2 and cards_list.count(cards_list[3]) == 2:  # 两对
            if cards_list.count(cards_list[0]) == 1:
                result = result | cards_list[3] << 8
                result = result | cards_list[1] << 4
                result = result | cards_list[0]
            elif cards_list.count(cards_list[2]) == 1:
                result = result | cards_list[3] << 8
                result = result | cards_list[1] << 4
                result = result | cards_list[2]
            elif cards_list.count(cards_list[4]) == 1:
                result = result | cards_list[3] << 8
                result = result | cards_list[1] << 4
                result = result | cards_list[4]
            result = 3 << 20 | result
        else:
            if cards_list[0] == cards_list[1]:  # 一对
                result = result | cards_list[0] << 12
                result = result | cards_list[4] << 8
                result = result | cards_list[3] << 4
                result = result | cards_list[2]
                result = 2 << 20 | result
            elif cards_list[1] == cards_list[2]:
                result = result | cards_list[1] << 12
                result = result | cards_list[4] << 8
                result = result | cards_list[3] << 4
                result = result | cards_list[0]
                result = 2 << 20 | result
            elif cards_list[2] == cards_list[3]:
                result = result | cards_list[2] << 12
                result = result | cards_list[4] << 8
                result = result | cards_list[1] << 4
                result = result | cards_list[0]
                result = 2 << 20 | result
            elif cards_list[3] == cards_list[4]:
                result = result | cards_list[3] << 12
                result = result | cards_list[2] << 8
                result = result | cards_list[1] << 4
                result = result | cards_list[0]
                result = 2 << 20 | result
            else:
                for i in range(5):
                    result = result | cards_list[i] << 4 * i
                result = 1 << 20 | result
        return result

    @staticmethod
    def rank(hand_card, public_card):
        max = 0
        poker_card = [hand_card[0], hand_card[1], public_card[0], public_card[1], public_card[2], public_card[3],
                      public_card[4]]
        for a in range(3):
            for b in range(a + 1, 4):
                for c in range(b + 1, 5):
                    for d in range(c + 1, 6):
                        for e in range(d + 1, 7):
                            temp = Player.get_card_power(poker_card[a][0], poker_card[b][0], poker_card[c][0],
                                                  poker_card[d][0], poker_card[e][0], poker_card[a][1],
                                                  poker_card[b][1], poker_card[c][1], poker_card[d][1],
                                                  poker_card[e][1])
                            if max < temp:
                                max = temp
        return max

    @staticmethod
    def is_higher_rank(rank1, rank2):
        from operator import itemgetter
        rank_list = (rank1, rank2)
        rank_list = sorted(rank_list, key=itemgetter(0, 1, 2, 3, 4, 5))
        if rank1 == rank_list[1]:
            return True
        return False
    @staticmethod
    def get_best_poker(player):#2张手牌与5张公共牌组合获取最好的  7*6/2 = 21
        poker_card = [player.hand[0],player.hand[1],Player.public_card[0],Player.public_card[1],Player.public_card[2],Player.public_card[3],Player.public_card[4]]
        for i in range(21):
            player.poker = []
            for j in range(5):
                player.poker.append(poker_card[Player.c_array[i][j]])
            player.poker.sort()
            player.cards_list = [card[0] for card in player.poker]  # 牌的大小
            player.suites_list = [card[1] for card in player.poker]  # 牌的花色
            player.synthesize()
            if i > 0 and Player.is_higher_rank(player.rank, player.best_rank):  # 当前牌型最佳
                player.best_rank = player.rank
            elif i == 0:
                player.best_rank = player.rank
    def flush(self):#同花
        if len(set(self.suites_list)) == 1:
            self.rank = ['6 flush', self.poker[4][0], self.poker[3][0], self.poker[2][0], self.poker[1][0],
                         self.poker[0][0]]
            return True
        return False

    def royal_flush(self):#同花顺
        cards_list = self.cards_list
        if len(set(self.suites_list)) == 1:
            if cards_list == [2, 3, 4, 5, 14]:
                self.rank = ["9 straight_flush", 5, 0, 0, 0, 0]
                return True
            for i in range(len(cards_list) - 1):
                if cards_list[i] + 1 != cards_list[i + 1]:
                    return False
            self.rank = ["9 straight_flush", self.poker[4][0], 0, 0, 0, 0]
            return True

    def straight(self):#顺子
        cards_list = self.cards_list
        if cards_list == [2, 3, 4, 5, 14]:
            self.rank = ['5 straight', 5, 0, 0, 0, 0]
            return True
        for i in range(len(cards_list) - 1):
            if cards_list[i] + 1 != cards_list[i + 1]:
                return False
        self.rank = ['5 straight', self.poker[4][0], 0, 0, 0, 0]
        return True

    def quadrant(self):#四条
        cards_list = self.cards_list
        if cards_list.count(cards_list[2]) == 4:
            self.rank = ['8 quadrant', self.poker[2][0], 0, 0, 0, 0]
            return True
        return False

    def full_house(self):#三带二
        cards_list = self.cards_list
        if cards_list.count(cards_list[0]) == 3 and cards_list.count(cards_list[4]) == 2:
            self.rank = ['7 full_house', self.poker[2][0], 0, 0, 0, 0]
            return True
        if cards_list.count(cards_list[0]) == 2 and cards_list.count(cards_list[4]) == 3:
            self.rank = ['7 full_house', self.poker[2][0], 0, 0, 0, 0]
            return True
        return False

    def triple(self):#三条
        cards_list = self.cards_list
        if cards_list.count(cards_list[2]) == 3:
            self.rank = ['4 triple', self.poker[2][0], 0, 0, 0, 0]
            return True
        return False

    def two_pairs(self):#两对
        cards_list = self.cards_list
        if cards_list.count(cards_list[1]) == 2 and cards_list.count(cards_list[3]) == 2:
            if cards_list.count(cards_list[4]) == 1:
                self.rank = ['3 two_pairs', self.poker[3][0], self.poker[1][0], self.poker[4][0], 0, 0]
                return True
            if cards_list.count(cards_list[2]) == 1:
                self.rank = ['3 two_pairs', self.poker[3][0], self.poker[1][0], self.poker[2][0], 0, 0]
                return True
            if cards_list.count(cards_list[0]) == 1:
                self.rank = ['3 two_pairs', self.poker[3][0], self.poker[1][0], self.poker[0][0], 0, 0]
                return True
        return False

    def pair(self):#一对
        if self.poker[0][0] == self.poker[1][0]:
            self.rank = ['2 pair', self.poker[1][0], self.poker[4][0], self.poker[3][0], self.poker[2][0], 0]
            return True
        if self.poker[1][0] == self.poker[2][0]:
            self.rank = ['2 pair', self.poker[2][0], self.poker[4][0], self.poker[3][0], self.poker[0][0], 0]
            return True
        if self.poker[2][0] == self.poker[3][0]:
            self.rank = ['2 pair', self.poker[3][0], self.poker[4][0], self.poker[1][0], self.poker[0][0], 0]
            return True
        if self.poker[3][0] == self.poker[4][0]:
            self.rank = ['2 pair', self.poker[3][0], self.poker[2][0], self.poker[1][0], self.poker[0][0], 0]
            return True
        return False

    def none(self):#高牌
        self.rank = ["1 none", self.poker[4][0], self.poker[3][0], self.poker[2][0], self.poker[1][0], self.poker[0][0]]

    def synthesize(self):#判断牌型
        if self.royal_flush() is True:
            return
        elif self.flush() is True:
            return
        elif self.quadrant() is True:
            return
        elif self.full_house() is True:
            return
        elif self.triple() is True:
            return
        elif self.two_pairs() is True:
            return
        elif self.pair() is True:
            return
        elif self.straight() is True:
            return
        else:
            self.none()