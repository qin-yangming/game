import socket
from threading import Thread
import re
from game_functions import *

# 登录
def login(data, tcp_socket):
    ret = re.match(r"登录成功！剩余筹码数为：([0-9]*)", data)
    if ret:
        print("等待他人进入游戏中···")
        return True
    if data == "用户密码错误，请重新输入" or data == "注册成功，请重新登录" or data == "请输入正确的账户和密码" or data == "该用户已登录！请换个用户试试":
        name = input("请输入你的用户名：")
        password = input("请输入你的密码：")
        s = '用户名：' + str(name) + "密码：" + str(password)
        tcp_socket.send(s.encode('utf-8'))
    ret = re.match(r"不存在该用户，是否注册该用户yes/no?", data)
    if ret:
        ans = input()
        tcp_socket.send(ans.encode('utf-8'))
    return False


def recv_msg(tcp_socket):
    is_login = 0
    name = input("请输入你的用户名：")
    password = input("请输入你的密码：")
    s = '用户名：' + str(name) + "密码：" + str(password)
    tcp_socket.send(s.encode('utf-8'))
    while True:
        data = tcp_socket.recv(1024)
        data = data.decode("utf-8")
        print(data) 
        if is_login == 0:
            if login(data, tcp_socket):
                is_login = 1
        elif is_login == 1:
            game_ready(data)


def game_ready(data):
    ret = re.match(r".*你的ID：([1-8])", data)
    if ret:
        Player.ID = int(ret.group(1))  # 获取自己的ID
    ret = re.match(r".*玩家名字：(.*)", data)
    if ret:
        temp = re.findall(r"\@(.*?)\@", ret[1])  # 名字
        Player.num = len(temp)
        for i in range(Player.num):
            print(temp[i])
            player[i + 1].name = temp[i]
        temp = re.findall(r"\$([0-9|\-]*)", ret[1])  # 筹码
        for i in range(Player.num):
            player[i + 1].money = int(temp[i])
        pos = play_pos_Dict[Player.num]
        for i in pos:
            create_card(player[0], 0, i, 2)
            create_card(player[0], 1, i, 2)
        for i in range(1, Player.num + 1):
            pos_dict[i] = pos[(i + Player.num - Player.ID) % Player.num]
            update_msg_money(i, 0)
        show_player_name()


def show_gui(tcp_socket):
    while True:
        check_events(tcp_socket, buttons)  # 鼠标键盘响应
        update_screen(screen, cards, buttons, msgs)  # 屏幕更新


def main():
    client_dic = {}
    with open('config.txt', 'r') as f:
        for line in f:
            key, value = line.strip().split(':')
            client_dic[key] = value
    print(client_dic)

    # 1.创建套接字
    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # 2.连接服务器
    tcp_socket.connect((client_dic['ip'], int(client_dic['port'])))
    # 3.创建一个线程与服务器进行通信
    p = Thread(target=recv_msg, args=(tcp_socket,))
    p.setDaemon(True)  # 设置守护线程
    p.start()  # 创建线程
    # 主线程用来界面显示
    show_gui(tcp_socket)


if __name__ == '__main__':
    main()
