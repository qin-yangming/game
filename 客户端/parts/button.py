import pygame.font
from pygame.sprite import Sprite
from pygame.sprite import Group
from parts.settings import *


class Button(Sprite):  # 按键
    def __init__(self, msg):
        super(Button, self).__init__()
        self.button_color = (0, 100, 0)  # 绿色  按钮背景颜色
        self.text_color = (255, 255, 255)  # 白色 文字颜色
        self.font = pygame.font.SysFont('SimHei', 36)  # 字体及大小
        #  创建按钮的 rect 对象，并使其居中
        """ 将 msg 渲染为图像，并使其在按钮上居中 """
        self.image = self.font.render(msg, True, self.text_color,
                                      self.button_color)
        self.rect = self.image.get_rect()

    def update_msg(self, msg):
        msg = str(msg)
        self.image = self.font.render(msg, True, self.text_color, self.button_color)


class Buttons(Group):
    def __init__(self, screen):
        super(Buttons, self).__init__()
        self.screen_rect = screen.get_rect()  # 获取屏幕矩形
        #  按钮
        self.play_button = Button("Start Game")  # 开始游戏按键
        self.restart_button = Button("Restart")  # 重新开始游戏按键
        self.bet_money_raise = Button(" + ")
        self.bet_money_down = Button(" - ")
        self.pass_button = Button("弃 牌")  # 弃牌
        self.call_button = Button("跟 注")  # 跟注
        self.Raise_button = Button("加 注")  # 加注
        self.play_button.rect.left = self.screen_rect.centerx + 290
        self.play_button.rect.centery = self.screen_rect.centery - 50
        self.pass_button.rect.centerx = self.screen_rect.centerx - 140
        self.pass_button.rect.bottom = self.call_button.rect.bottom = self.Raise_button.rect.bottom = bottom_y1 - card_height - 60
        self.call_button.rect.centerx = self.screen_rect.centerx
        self.Raise_button.rect.centerx = self.screen_rect.centerx + 140
        self.bet_money_raise.rect.centerx = self.screen_rect.centerx + 150
        self.bet_money_raise.rect.bottom = self.screen_rect.bottom - 140
        self.bet_money_down.rect.centerx = self.screen_rect.centerx + 150
        self.bet_money_down.rect.bottom = self.screen_rect.bottom - 60
        self.add(self.play_button)

    def kill_buttons(self):
        self.pass_button.kill()
        self.call_button.kill()
        self.Raise_button.kill()
        self.bet_money_raise.kill()
        self.bet_money_down.kill()

    def add_buttons(self):
        self.add(self.pass_button)
        self.add(self.call_button)
        self.add(self.Raise_button)
        self.add(self.bet_money_raise)
        self.add(self.bet_money_down)
